// 
// input: - Chieu dai HCN: 8cm
//        - Chieu rong HCN: 6cm
// 
//  Step: - Tạo 2 biến lưu input
//        - Tạo 2 biến lưu Output
//        - Tính theo công thức: + Dien tich HCN: Chieu dai x Chieu rong
//                               + Chu vi HCN: (Chieu dai x Chieu rong) x 2
// Output:- Dien tich HCN: 48cm
//        - Chu vi HCN:28cm


var Chieudai = 8;
var Chieurong = 6;
var DientichHCN = null;
var ChuviHCN = null;

DientichHCN = Chieudai * Chieurong;
ChuviHCN = (Chieudai + Chieurong)*2;

console.log({
    DientichHCN,
    ChuviHCN
})